use crate::input;

use itertools::Itertools;

///
///  aaaa
/// b    c
/// b    c
///  dddd
/// e    f  
/// e    f
///  gggg
///
///   0:      1:      2:      3:      4:
///  aaaa    ....    aaaa    aaaa    ....
/// b    c  .    c  .    c  .    c  b    c
/// b    c  .    c  .    c  .    c  b    c
///  ....    ....    dddd    dddd    dddd
/// e    f  .    f  e    .  .    f  .    f
/// e    f  .    f  e    .  .    f  .    f
///  gggg    ....    gggg    gggg    ....
///
///   5:      6:      7:      8:      9:
///  aaaa    aaaa    aaaa    aaaa    aaaa
/// b    .  b    .  .    c  b    c  b    c
/// b    .  b    .  .    c  b    c  b    c
///  dddd    dddd    ....    dddd    dddd
/// .    f  e    f  .    f  e    f  .    f
/// .    f  e    f  .    f  e    f  .    f
///  gggg    gggg    ....    gggg    gggg

pub fn run() {
  println!("============ Day 8 ============");
  let lines: Vec<(Vec<String>, Vec<String>)> = input::get_lines("day08")
    .iter()
    // split the input in the two parts it is composed of
    .map(|s| s.split(" | ").map(String::from).collect::<Vec<String>>())
    // then split these two parts into the various "words"
    .map(|s| {
      (
        s[0]
          .split(' ')
          // sort each word in alphabetical order and convert to String
          .map(|s| s.chars().sorted().collect::<String>())
          .collect(),
        s[1]
          .split(' ')
          // sort each word in alphabetical order and convert to String
          .map(|s| s.chars().sorted().collect::<String>())
          .collect(),
      )
    })
    .collect();
  println!("Part 1: {}", part1(&lines));
  println!("Part 2: {}", part2(&lines));
}

fn part1(lines: &Vec<(Vec<String>, Vec<String>)>) -> u32 {
  let mut count = 0;
  for line in lines {
    for segment in &line.1 {
      if segment.len() == 2 || segment.len() == 3 || segment.len() == 4 || segment.len() == 7 {
        count += 1;
      }
    }
  }
  count
}

fn part2(lines: &Vec<(Vec<String>, Vec<String>)>) -> u32 {
  let mut count: u32 = 0;
  for line in lines {
    let mut segments = ["", "", "", "", "", "", "", "", "", ""];
    // these 4 digits are easy to find, since they are the only ones
    // to have their respective number of segments
    segments[1] = line.0.iter().find(|s| s.len() == 2).unwrap();
    segments[4] = line.0.iter().find(|s| s.len() == 4).unwrap();
    segments[7] = line.0.iter().find(|s| s.len() == 3).unwrap();
    segments[8] = line.0.iter().find(|s| s.len() == 7).unwrap();
    // 9 is the only digit with 6 segments to contain every segment 4 has
    for digit in &line.0 {
      if digit.len() == 6 {
        if contains_all_chars(digit, &segments[4].to_string()) {
          segments[9] = digit;
        }
      }
    }
    // 0 is the only digit with 6 segments, other than 9, to contain every segment 1 has
    for digit in &line.0 {
      if digit.len() == 6 && digit != &segments[9] {
        if contains_all_chars(digit, &segments[1].to_string()) {
          segments[0] = digit;
        }
      }
    }
    // 6 is the only other digit with 6 segments
    for digit in &line.0 {
      if digit.len() == 6 && digit != &segments[9] && digit != &segments[0] {
        segments[6] = digit;
      }
    }
    // 5 is the only 5-segments digit to be completely contained in 6
    for digit in &line.0 {
      if digit.len() == 5 {
        if contains_all_chars(&segments[6].to_string(), digit) {
          segments[5] = digit;
        }
      }
    }
    // 3 is the only 5-segments digit, other than 5, to be completely contained in 9
    for digit in &line.0 {
      if digit.len() == 5 && digit != &segments[5] {
        if contains_all_chars(&segments[9].to_string(), digit) {
          segments[3] = digit;
        }
      }
    }
    // 2 is the only other 5-segments digit
    for digit in &line.0 {
      if digit.len() == 5 && digit != &segments[5] && digit != &segments[3] {
        segments[2] = digit;
      }
    }
    // now we reconstruct the number, keeping in mind that it is base 10
    let mut num = 0;
    for digit in &line.1 {
      // here we need the words to be sorted because,
      // although we don't care about it, the equality operator does
      num = num * 10 + segments.iter().position(|&r| r == digit).unwrap();
    }
    count += num as u32;
  }
  count
}

fn contains_all_chars(s1: &String, s2: &String) -> bool {
  for c in s2.chars() {
    if !s1.contains(c) {
      return false;
    }
  }
  true
}
