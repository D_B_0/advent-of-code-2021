use crate::input;

type Image = Vec<Vec<bool>>;
type Algorithm = Vec<bool>;

pub fn run() {
  println!("============ Day 20 ============");
  let content = input::get_contents("day20");
  let mut lines = content.lines();
  let algo: Algorithm = lines.next().unwrap().chars().map(|c| c == '#').collect();
  let mut image: Image = lines
    .skip(1)
    .map(|l| l.chars().map(|c| c == '#').collect())
    .collect();
  // display_image(&image);
  // print!("\n");
  for i in 0..50 {
    image = apply_algo(
      &image,
      &algo,
      if i % 2 == 1 {
        algo[0]
      } else {
        algo[algo.len() - 1]
      },
    );
    if i == 1 {
      println!(
        "Part 1: {}",
        image
          .iter()
          .map(|l| l.iter().filter(|&&b| b).count())
          .sum::<usize>()
      )
    }
  }
  println!(
    "Part 2: {}",
    image
      .iter()
      .map(|l| l.iter().filter(|&&b| b).count())
      .sum::<usize>()
  );
}

#[allow(dead_code)]
fn display_image(image: &Image) {
  for line in image {
    for &val in line {
      if val {
        print!("#");
      } else {
        print!(".");
      }
    }
    print!("\n");
  }
}

fn apply_algo(image: &Image, algo: &Algorithm, default: bool) -> Image {
  let mut new_image = Vec::with_capacity(image.len() + 2);
  for i in 0..image.len() as i32 + 2 {
    let mut line = Vec::with_capacity(image[0].len() + 2);
    for j in 0..image[0].len() as i32 + 2 {
      let mut res = 0usize;
      for di in -1..=1 {
        for dj in -1..=1 {
          res = (res << 1)
            + if get_or(&image, j + dj - 1, i + di - 1, default) {
              1
            } else {
              0
            };
        }
      }
      // line.push(get_or(&image, j - 1, i - 1, false));
      line.push(algo[res]);
    }
    new_image.push(line);
  }
  new_image
}

fn get_or(image: &Image, x: i32, y: i32, default: bool) -> bool {
  if x < 0 || x > image[0].len() as i32 - 1 {
    return default;
  }
  if y < 0 || y > image.len() as i32 - 1 {
    return default;
  }
  image[y as usize][x as usize]
}
