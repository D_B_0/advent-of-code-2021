use crate::input;

use colored::*;
use regex::Regex;

#[derive(Debug)]
struct Point {
  pub x: u32,
  pub y: u32,
}

#[derive(Debug)]
struct Line {
  pub p1: Point,
  pub p2: Point,
}

#[derive(Debug)]
enum Direction {
  Vertical,
  Horizontal,
  Diagonal,
}

impl Line {
  fn direction(&self) -> Direction {
    if self.p1.x == self.p2.x {
      Direction::Vertical
    } else if self.p1.y == self.p2.y {
      Direction::Horizontal
    } else {
      Direction::Diagonal
    }
  }

  fn range_x(&self) -> std::ops::RangeInclusive<u32> {
    if self.p1.x > self.p2.x {
      self.p2.x..=self.p1.x
    } else {
      self.p1.x..=self.p2.x
    }
  }

  fn range_y(&self) -> std::ops::RangeInclusive<u32> {
    if self.p1.y > self.p2.y {
      self.p2.y..=self.p1.y
    } else {
      self.p1.y..=self.p2.y
    }
  }
}

pub fn run() {
  println!("============ Day 5 ============");
  let lines = input::get_lines("day05");
  let re = Regex::new(r"(?P<x1>\d+),(?P<y1>\d+) -> (?P<x2>\d+),(?P<y2>\d+)").unwrap();
  let mut vent_lines = Vec::<Line>::new();
  for line in lines {
    for caps in re.captures_iter(&line) {
      vent_lines.push(Line {
        p1: Point {
          x: caps["x1"].parse().unwrap(),
          y: caps["y1"].parse().unwrap(),
        },
        p2: Point {
          x: caps["x2"].parse().unwrap(),
          y: caps["y2"].parse().unwrap(),
        },
      });
    }
  }
  println!("Part 1: {}", part1(&vent_lines));
  println!("Part 2: {}", part2(&vent_lines));
}

fn part1(lines: &Vec<Line>) -> u32 {
  let mut max_x = 0;
  let mut max_y = 0;

  for Line { p1, p2 } in lines {
    if p1.x > max_x {
      max_x = p1.x
    }
    if p2.x > max_x {
      max_x = p2.x
    }
    if p1.y > max_y {
      max_y = p1.y
    }
    if p2.y > max_y {
      max_y = p2.y
    }
  }
  max_x += 1;
  max_y += 1;

  let mut grid = Vec::<u32>::with_capacity((max_x * max_y) as usize);
  for _ in 0..max_x * max_y {
    grid.push(0);
  }

  for line in lines {
    match line.direction() {
      Direction::Vertical => {
        for y in line.range_y() {
          grid[(line.p1.x + y * max_x) as usize] += 1;
        }
      }
      Direction::Horizontal => {
        for x in line.range_x() {
          grid[(x + line.p1.y * max_x) as usize] += 1;
        }
      }
      _ => {}
    }
  }

  grid
    .iter()
    .fold(0, |acc, &x| if x >= 2 { acc + 1 } else { acc })
}

fn part2(lines: &Vec<Line>) -> u32 {
  let mut max_x = 0;
  let mut max_y = 0;

  for Line { p1, p2 } in lines {
    if p1.x > max_x {
      max_x = p1.x
    }
    if p2.x > max_x {
      max_x = p2.x
    }
    if p1.y > max_y {
      max_y = p1.y
    }
    if p2.y > max_y {
      max_y = p2.y
    }
  }
  max_x += 1;
  max_y += 1;

  let mut grid = Vec::<u32>::with_capacity((max_x * max_y) as usize);
  for _ in 0..max_x * max_y {
    grid.push(0);
  }

  for line in lines {
    match line.direction() {
      Direction::Vertical => {
        for y in line.range_y() {
          grid[(line.p1.x + y * max_x) as usize] += 1;
        }
      }
      Direction::Horizontal => {
        for x in line.range_x() {
          grid[(x + line.p1.y * max_x) as usize] += 1;
        }
      }
      Direction::Diagonal => {
        for i in 0..=(line.p1.x as i32 - line.p2.x as i32).abs() as u32 {
          let x: u32;
          if line.p1.x < line.p2.x {
            x = line.p1.x + i;
          } else {
            x = line.p1.x - i;
          }
          let y: u32;
          if line.p1.y < line.p2.y {
            y = line.p1.y + i;
          } else {
            y = line.p1.y - i;
          }
          grid[(x + y * max_x) as usize] += 1;
        }
      }
    }
  }

  grid
    .iter()
    .fold(0, |acc, &x| if x >= 2 { acc + 1 } else { acc })
}

#[allow(dead_code)]
fn print_grid(grid: &Vec<u32>, max_x: u32, max_y: u32) {
  for y in 0..max_y {
    for x in 0..max_x {
      print!(
        "{}",
        if grid[(x + y * max_x) as usize] == 0 {
          ".".to_owned().bright_black()
        } else if grid[(x + y * max_x) as usize] > 1 {
          grid[(x + y * max_x) as usize].to_string().red()
        } else {
          grid[(x + y * max_x) as usize].to_string().white()
        }
      );
    }
    print!("\n");
  }
}
