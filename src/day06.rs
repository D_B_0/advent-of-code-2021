use crate::input;

pub fn run() {
  println!("============ Day 6 ============");
  let nums: Vec<u64> = input::get_contents("day06")
    .split(',')
    .map(|s| s.parse().unwrap())
    .collect();
  let mut occurences = vec![0, 0, 0, 0, 0, 0, 0, 0, 0];
  for n in &nums {
    occurences[*n as usize] += 1;
  }

  for _ in 0..80 {
    let mut new_occ = occurences.clone();
    new_occ[8] = occurences[0];
    new_occ[7] = occurences[8];
    new_occ[6] = occurences[0] + occurences[7];
    new_occ[5] = occurences[6];
    new_occ[4] = occurences[5];
    new_occ[3] = occurences[4];
    new_occ[2] = occurences[3];
    new_occ[1] = occurences[2];
    new_occ[0] = occurences[1];
    occurences = new_occ;
  }
  println!("Part 1: {}", occurences.iter().sum::<u64>());
  for _ in 0..(256 - 80) {
    let mut new_occ = occurences.clone();
    new_occ[8] = occurences[0];
    new_occ[7] = occurences[8];
    new_occ[6] = occurences[0] + occurences[7];
    new_occ[5] = occurences[6];
    new_occ[4] = occurences[5];
    new_occ[3] = occurences[4];
    new_occ[2] = occurences[3];
    new_occ[1] = occurences[2];
    new_occ[0] = occurences[1];
    occurences = new_occ;
  }
  println!("Part 2: {}", occurences.iter().sum::<u64>());
}
