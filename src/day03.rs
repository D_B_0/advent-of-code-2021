use crate::input;

pub fn run() {
  println!("============ Day 3 ============");
  let lines = input::get_lines("day03");
  println!("Part 1: {}", part1(&lines));
  println!("Part 2: {}", part2(&lines));
}

fn part1(lines: &Vec<String>) -> u32 {
  let mut gamma: String = String::new();
  let mut epsilon: String = String::new();
  for i in 0..lines[0].len() {
    if most_common_char(lines, i) == '1' {
      gamma += "1";
      epsilon += "0";
    } else {
      epsilon += "1";
      gamma += "0";
    }
  }

  let gamma_int = str_to_u32(gamma.as_str());
  let epsilon_int = str_to_u32(epsilon.as_str());

  gamma_int * epsilon_int
}

fn part2(lines: &Vec<String>) -> u32 {
  let ox_gen_rating = filter_bins(lines, true);
  let co2_scrub_rating = filter_bins(lines, false);

  let ox_gen_int = str_to_u32(ox_gen_rating.as_str());
  let co2_scrub_int = str_to_u32(co2_scrub_rating.as_str());

  ox_gen_int * co2_scrub_int
}

fn filter_bins(candidates: &Vec<String>, equal: bool) -> String {
  let mut filtered_candidates = candidates.clone();
  for i in 0..filtered_candidates[0].len() {
    let mut new_candidates = Vec::<String>::new();
    let most_common = most_common_char(&filtered_candidates, i);
    for s in filtered_candidates {
      match equal {
        true => {
          if s.chars().nth(i).unwrap() == most_common {
            new_candidates.push(s.clone());
          }
        }
        false => {
          if s.chars().nth(i).unwrap() != most_common {
            new_candidates.push(s.clone());
          }
        }
      }
    }

    filtered_candidates = new_candidates;
    if filtered_candidates.len() == 1 {
      break;
    }
  }
  filtered_candidates[0].clone()
}

fn str_to_u32(s: &str) -> u32 {
  u32::from_str_radix(s, 2).unwrap()
}

fn most_common_char(lines: &Vec<String>, index: usize) -> char {
  let mut count1 = 0;
  for line in lines {
    if line.chars().nth(index).unwrap() == '1' {
      count1 += 1;
    }
  }
  if count1 as f32 >= lines.len() as f32 / 2.0 {
    '1'
  } else {
    '0'
  }
}
