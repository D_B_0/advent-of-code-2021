#![allow(dead_code)]

use crate::input;

struct Scanner {
  id: usize,
  beacons: Vec<Vector>,
}

impl Scanner {
  fn relationships(&self) -> Vec<(Vector, usize, usize)> {
    let mut res = Vec::new();
    for idx1 in 0..self.beacons.len() {
      let beacon1 = self.beacons[idx1];
      for idx2 in idx1 + 1..self.beacons.len() {
        let beacon2 = self.beacons[idx2];
        let sub = (beacon1 - beacon2).abs();
        // if !res.contains(&(sub, _, _)) {
        res.push((sub, idx1, idx2));
        // }
      }
    }
    res
  }

  fn count_relationships(&self, other: &Self) -> usize {
    let others_relationships = other.relationships();
    let relationships = self.relationships();
    let res = relationships.iter().filter(|&&v| {
      others_relationships
        .iter()
        .position(|v2| v2.0 == v.0)
        .is_some()
    });
    let mut indices: Vec<usize> = res.clone().map(|v| v.1).collect();
    indices.extend(res.map(|v| v.2));
    indices.sort();
    indices.dedup();
    // println!("{:?}", indices);
    let mut count = 0;
    for _ in indices {
      // println!("{:?}", val);
      count += 1;
    }
    count
  }
}

#[allow(unreachable_code)]
pub fn run() {
  println!("============ Day 19 ============");
  println!("I give up");
  return;
  let content = input::get_contents_example("day19").replace("---", "");
  let scanners: Vec<Scanner> = content
    .split(" scanner")
    .skip(1)
    .map(|scanner| {
      let mut lines = scanner.lines();
      Scanner {
        id: lines.next().unwrap().trim().parse::<usize>().unwrap(),
        beacons: lines.filter(|&l| l != "").map(parse_coords).collect(),
      }
    })
    .collect();

  for idx1 in 0..scanners.len() {
    for idx2 in idx1 + 1..scanners.len() {
      let count = scanners[idx2].count_relationships(&scanners[idx2]);
      println!("{} to {}, {} relationships", idx1, idx2, count);
    }
  }
  // println!("{}", scanners[0].count_relationships(&scanners[1]));
}

#[derive(Debug, PartialEq, Eq, Copy, Clone)]
struct Vector {
  x: i64,
  y: i64,
  z: i64,
}

impl Vector {
  fn abs(&self) -> Self {
    Self {
      x: self.x.abs(),
      y: self.y.abs(),
      z: self.z.abs(),
    }
  }

  fn eq(&self, other: Self) -> bool {
    (self.x == other.x && self.y == other.y && self.z == other.z)
      || (self.x == other.x && self.y == other.z && self.z == other.z)
      || (self.x == other.y && self.y == other.x && self.z == other.z)
      || (self.x == other.y && self.y == other.z && self.z == other.x)
      || (self.x == other.z && self.y == other.y && self.z == other.x)
      || (self.x == other.z && self.y == other.x && self.z == other.y)
  }
}

impl std::fmt::Display for Vector {
  fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
    write!(f, "x: {}, y: {}, z: {}", self.x, self.y, self.z)
  }
}

impl std::ops::Sub for Vector {
  type Output = Self;

  fn sub(self, other: Self) -> Self::Output {
    Self {
      x: self.x - other.x,
      y: self.y - other.y,
      z: self.z - other.z,
    }
  }
}

fn parse_coords(string: &str) -> Vector {
  let (x, rest) = string.split_once(",").unwrap();
  let (y, z) = rest.split_once(",").unwrap();
  Vector {
    x: x.parse().unwrap(),
    y: y.parse().unwrap(),
    z: z.parse().unwrap(),
  }
}
// type Mat3x3 = [[i64; 3]; 3];

// const RX0: Mat3x3 = [[1, 0, 0], [0, 1, 0], [0, 0, 1]];
// const RY0: Mat3x3 = RX0;
// const RZ0: Mat3x3 = RX0;

// const RX90: Mat3x3 = [[1, 0, 0], [0, 0, 1], [0, -1, 0]];
// const RY90: Mat3x3 = [[0, 0, -1], [0, 1, 0], [1, 0, 0]];
// const RZ90: Mat3x3 = [[0, 1, 0], [-1, 0, 0], [0, 0, 1]];

// const RX180: Mat3x3 = [[1, 0, 0], [0, -1, 0], [0, 0, -1]];
// const RY180: Mat3x3 = [[-1, 0, 0], [0, 1, 0], [0, 0, -1]];
// const RZ180: Mat3x3 = [[-1, 0, 0], [0, -1, 0], [0, 0, 1]];

// const RX270: Mat3x3 = [[1, 0, 0], [0, 0, -1], [0, 1, 0]];
// const RY270: Mat3x3 = [[0, 0, 1], [0, 1, 0], [-1, 0, 0]];
// const RZ270: Mat3x3 = [[0, -1, 0], [1, 0, 0], [0, 0, 1]];

// fn matrix_vec_mult(mat: Mat3x3, vec: Vector) -> Vector {
//   Vector {
//     x: vec.x * mat[0][0] + vec.y * mat[1][0] + vec.z * mat[2][0],
//     y: vec.x * mat[0][1] + vec.y * mat[1][1] + vec.z * mat[2][1],
//     z: vec.x * mat[0][2] + vec.y * mat[1][2] + vec.z * mat[2][2],
//   }
// }

// fn print_mat3x3(mat: Mat3x3) {
//   println!("┏ {:2}, {:2}, {:2} ┓", mat[0][0], mat[1][0], mat[2][0]);
//   println!("┃ {:2}, {:2}, {:2} ┃", mat[0][1], mat[1][1], mat[2][1]);
//   println!("┗ {:2}, {:2}, {:2} ┛", mat[0][2], mat[1][2], mat[2][2]);
// }
