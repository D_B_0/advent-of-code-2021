use crate::input;

#[derive(PartialEq, Eq)]
enum Bracket {
  Pranthesis,
  Square,
  Curly,
  Angled,
}

const OPENING_BRACKETS: &str = "([{<";
const CLOSING_BRACKETS: &str = ")]}>";

pub fn run() {
  println!("============ Day 10 ============");
  let mut lines = input::get_lines("day10");
  println!("Part 1: {}", part1(&mut lines));
  println!("Part 2: {}", part2(&lines));
}

fn part1(lines: &mut Vec<String>) -> u32 {
  let mut points = 0;
  let mut incomplete_lines = Vec::<String>::new();
  for line in lines.iter() {
    let mut chunks_in_progress = Vec::<Bracket>::new();
    let mut valid = true;
    for c in line.chars() {
      if OPENING_BRACKETS.contains(c) {
        chunks_in_progress.push(match c {
          '(' => Bracket::Pranthesis,
          '[' => Bracket::Square,
          '{' => Bracket::Curly,
          _ => Bracket::Angled,
        });
      } else if CLOSING_BRACKETS.contains(c) {
        let actual = match c {
          ')' => Bracket::Pranthesis,
          ']' => Bracket::Square,
          '}' => Bracket::Curly,
          _ => Bracket::Angled,
        };
        let expected = chunks_in_progress.last().unwrap();
        if &actual == expected {
          chunks_in_progress.pop();
        } else {
          points += match actual {
            Bracket::Pranthesis => 3,
            Bracket::Square => 57,
            Bracket::Curly => 1197,
            Bracket::Angled => 25137,
          };
          valid = false;
          break;
        }
      }
    }
    if valid {
      incomplete_lines.push(line.clone());
    }
  }
  lines.clear();
  for line in incomplete_lines {
    lines.push(line.clone());
  }
  points
}

fn part2(lines: &Vec<String>) -> u64 {
  let mut scores = Vec::<u64>::new();
  for line in lines {
    let mut chunks_in_progress = Vec::<Bracket>::new();
    for c in line.chars() {
      if OPENING_BRACKETS.contains(c) {
        chunks_in_progress.push(match c {
          '(' => Bracket::Pranthesis,
          '[' => Bracket::Square,
          '{' => Bracket::Curly,
          _ => Bracket::Angled,
        });
      } else if CLOSING_BRACKETS.contains(c) {
        let actual = match c {
          ')' => Bracket::Pranthesis,
          ']' => Bracket::Square,
          '}' => Bracket::Curly,
          _ => Bracket::Angled,
        };
        let expected = chunks_in_progress.last().unwrap();
        if &actual == expected {
          chunks_in_progress.pop();
        }
      }
    }
    let mut complete_line = "".to_string();
    let mut score = 0;
    loop {
      if let Some(b) = chunks_in_progress.pop() {
        complete_line.push(get_closing_bracket(&b));
        score *= 5;
        score += match b {
          Bracket::Pranthesis => 1,
          Bracket::Square => 2,
          Bracket::Curly => 3,
          Bracket::Angled => 4,
        };
      } else {
        break;
      }
    }
    // println!("{:25} {}, score: {}", line, complete_line, score);
    scores.push(score);
  }
  scores.sort();
  scores[scores.len() / 2]
}

#[allow(dead_code)]
fn get_closing_bracket(bracket: &Bracket) -> char {
  match bracket {
    Bracket::Pranthesis => ')',
    Bracket::Square => ']',
    Bracket::Curly => '}',
    Bracket::Angled => '>',
  }
}

#[allow(dead_code)]
fn get_opening_bracket(bracket: &Bracket) -> char {
  match bracket {
    Bracket::Pranthesis => '(',
    Bracket::Square => '[',
    Bracket::Curly => '{',
    Bracket::Angled => '<',
  }
}
