use crate::input;
use colored::*;
use std::fmt;

#[derive(Debug, Copy, Clone)]
struct Board {
  pub grid: [[u32; 5]; 5],
  pub marks: [[bool; 5]; 5],
}

impl Board {
  fn new(nums: Vec<u32>) -> Self {
    assert_eq!(
      nums.len(),
      25,
      "Not enough/Too many elements to make a Board"
    );
    let mut grid: [[u32; 5]; 5] = [[0; 5]; 5];

    for i in 0..nums.len() {
      grid[i / 5][i % 5] = nums[i];
    }

    Board {
      grid,
      marks: Default::default(),
    }
  }

  fn mark(&mut self, num: &u32) {
    for y in 0..5 {
      for x in 0..5 {
        if self.grid[y][x] == *num {
          self.marks[y][x] = true;
        }
      }
    }
  }

  fn won(&self) -> bool {
    for y in 0..5 {
      if self.marks[y][0]
        && self.marks[y][1]
        && self.marks[y][2]
        && self.marks[y][3]
        && self.marks[y][4]
      {
        return true;
      }
    }
    for x in 0..5 {
      if self.marks[0][x]
        && self.marks[1][x]
        && self.marks[2][x]
        && self.marks[3][x]
        && self.marks[4][x]
      {
        return true;
      }
    }
    false
  }

  fn sum_unmarked(&self) -> u32 {
    let mut sum = 0;
    for y in 0..5 {
      for x in 0..5 {
        if !self.marks[y][x] {
          sum += self.grid[y][x];
        }
      }
    }
    sum
  }
}

impl fmt::Display for Board {
  // This trait requires `fmt` with this exact signature.
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    write!(
      f,
      "{}",
      self
        .grid
        .iter()
        .enumerate()
        .map(|(y, line)| line
          .iter()
          .enumerate()
          .map(|(x, n)| if self.marks[y][x] {
            format!("{:2}", n).green().to_string()
          } else {
            format!("{:2}", n).white().to_string()
          })
          .collect::<Vec<String>>()
          .join(" "))
        .collect::<Vec<String>>()
        .join("\n")
    )
  }
}

pub fn run() {
  println!("============ Day 4 ============");

  let file = input::get_contents("day04");
  let inputs: Vec<&str>;
  if cfg!(windows) {
    inputs = file.split("\r\n\r\n").collect();
  } else {
    inputs = file.split("\n\n").collect();
  }
  assert!(inputs.len() >= 2, "Invalid input.");

  let nums: Vec<u32> = inputs[0]
    .split(',')
    .map(|s| s.parse::<u32>().expect("Invalid number extracted"))
    .collect();
  let boards: Vec<Board> = inputs
    .clone()
    .drain(1..)
    .map(|b| {
      Board::new(
        b.split_whitespace()
          .map(|s| s.parse::<u32>().expect("Invalid number in board"))
          .collect(),
      )
    })
    .collect();

  println!("Part 1:");
  println!("Solution: {}", part1(&nums, &boards));
  println!("Part 2:");
  println!("Solution: {}", part2(&nums, &boards));
}

fn part1(nums: &Vec<u32>, boards_orig: &Vec<Board>) -> u32 {
  let mut boards = boards_orig.clone();
  for n in nums {
    for i in 0..boards.len() {
      boards[i].mark(n);
      if boards[i].won() {
        println!("Winner board:\n{}", boards[i]);
        println!("Last number extracted: {}", n);
        println!("Sum of unmarked numbers: {}", boards[i].sum_unmarked());
        return n * boards[i].sum_unmarked();
      }
    }
  }
  0
}

fn part2(nums: &Vec<u32>, boards_orig: &Vec<Board>) -> u32 {
  let mut uncompleted_boards = boards_orig.clone();
  let mut completed_boards = Vec::<Board>::new();
  for n in nums {
    for i in 0..uncompleted_boards.len() {
      uncompleted_boards[i].mark(n);
    }
    let mut i = 0;
    let mut max = uncompleted_boards.len();
    while i < max {
      if uncompleted_boards[i].won() {
        completed_boards.push(uncompleted_boards[i]);
        uncompleted_boards.remove(i);
        max -= 1;
      }
      i += 1;
    }
    if uncompleted_boards.len() == 0 {
      let board = completed_boards.last().unwrap();
      println!("Winner board:\n{}", board);
      println!("Last number extracted: {}", n);
      println!("Sum of unmarked numbers: {}", board.sum_unmarked());
      return board.sum_unmarked() * n;
    }
  }
  0
}
