use crate::input;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Space {
  Empty,
  Wall,
  Outside,
  Amber,
  Bronze,
  Copper,
  Desert,
}

fn space_from_char(c: char) -> Space {
  match c {
    'A' => Space::Amber,
    'B' => Space::Bronze,
    'C' => Space::Copper,
    'D' => Space::Desert,
    _ => unreachable!(),
  }
}

type Board = [[Space; 13]; 5];

pub fn run() {
  println!("============ Day 23 ============");
  let content = input::get_contents("day23");
  let mut spaces = content
    .chars()
    .filter(|&c| "ABCD".contains(c))
    .map(space_from_char);
  let mut board = make_board(
    spaces.next().unwrap(),
    spaces.next().unwrap(),
    spaces.next().unwrap(),
    spaces.next().unwrap(),
    spaces.next().unwrap(),
    spaces.next().unwrap(),
    spaces.next().unwrap(),
    spaces.next().unwrap(),
  );
  let mut energy_spent = 0;
  move_space((9, 2), (10, 1), &mut board, &mut energy_spent);
  move_space((9, 3), (2, 1), &mut board, &mut energy_spent);
  move_space((5, 2), (4, 1), &mut board, &mut energy_spent);
  move_space((5, 3), (6, 1), &mut board, &mut energy_spent);
  move_space((6, 1), (9, 3), &mut board, &mut energy_spent);
  move_space((10, 1), (9, 2), &mut board, &mut energy_spent);
  move_space((4, 1), (5, 3), &mut board, &mut energy_spent);
  move_space((2, 1), (5, 2), &mut board, &mut energy_spent);
  move_space((3, 2), (10, 1), &mut board, &mut energy_spent);
  move_space((3, 3), (8, 1), &mut board, &mut energy_spent);
  move_space((7, 2), (7, 2), &mut board, &mut energy_spent);
  move_space((7, 2), (3, 3), &mut board, &mut energy_spent);
  move_space((7, 3), (7, 2), &mut board, &mut energy_spent);
  move_space((7, 2), (3, 2), &mut board, &mut energy_spent);
  move_space((8, 1), (7, 3), &mut board, &mut energy_spent);
  move_space((10, 1), (7, 2), &mut board, &mut energy_spent);
  // print_board(&board);
}

fn move_space(
  from: (usize, usize),
  to: (usize, usize),
  board: &mut Board,
  energy_spent: &mut usize,
) {
  if board[to.1][to.0] != Space::Empty {
    return;
  }
  if to.1 == 1 {
    if to.0 == 3 || to.0 == 5 || to.0 == 7 || to.0 == 9 {
      return;
    }
  }
  let space_to_move = board[from.1][from.0];
  if space_to_move != Space::Amber
    && space_to_move != Space::Bronze
    && space_to_move != Space::Copper
    && space_to_move != Space::Desert
  {
    return;
  }
  *energy_spent += manhattan_distance(from, to)
    * match space_to_move {
      Space::Amber => 1,
      Space::Bronze => 10,
      Space::Copper => 100,
      Space::Desert => 1000,
      _ => unreachable!(),
    };
  board[from.1][from.0] = Space::Empty;
  board[to.1][to.0] = space_to_move;
  print_board(board);
  println!("Energy spent: {}", energy_spent);
}

fn manhattan_distance(from: (usize, usize), to: (usize, usize)) -> usize {
  from.0.max(to.0) - from.0.min(to.0) + from.1.max(to.1) - from.1.min(to.1)
}

fn make_board(
  space1: Space,
  space2: Space,
  space3: Space,
  space4: Space,
  space5: Space,
  space6: Space,
  space7: Space,
  space8: Space,
) -> Board {
  [
    [
      Space::Wall,
      Space::Wall,
      Space::Wall,
      Space::Wall,
      Space::Wall,
      Space::Wall,
      Space::Wall,
      Space::Wall,
      Space::Wall,
      Space::Wall,
      Space::Wall,
      Space::Wall,
      Space::Wall,
    ],
    [
      Space::Wall,
      Space::Empty,
      Space::Empty,
      Space::Empty,
      Space::Empty,
      Space::Empty,
      Space::Empty,
      Space::Empty,
      Space::Empty,
      Space::Empty,
      Space::Empty,
      Space::Empty,
      Space::Wall,
    ],
    [
      Space::Wall,
      Space::Wall,
      Space::Wall,
      space1,
      Space::Wall,
      space2,
      Space::Wall,
      space3,
      Space::Wall,
      space4,
      Space::Wall,
      Space::Wall,
      Space::Wall,
    ],
    [
      Space::Outside,
      Space::Outside,
      Space::Wall,
      space5,
      Space::Wall,
      space6,
      Space::Wall,
      space7,
      Space::Wall,
      space8,
      Space::Wall,
      Space::Outside,
      Space::Outside,
    ],
    [
      Space::Outside,
      Space::Outside,
      Space::Wall,
      Space::Wall,
      Space::Wall,
      Space::Wall,
      Space::Wall,
      Space::Wall,
      Space::Wall,
      Space::Wall,
      Space::Wall,
      Space::Outside,
      Space::Outside,
    ],
  ]
}

fn print_board(board: &Board) {
  for line in board {
    for &space in line {
      print!(
        "{}",
        match space {
          Space::Empty => ".",
          Space::Wall => "#",
          Space::Outside => " ",
          Space::Amber => "A",
          Space::Bronze => "B",
          Space::Copper => "C",
          Space::Desert => "D",
        }
      );
    }
    print!("\n");
  }
}
