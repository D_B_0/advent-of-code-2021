use crate::input;

use std::collections::HashMap;

pub fn run() {
  println!("============ Day 14 ============");
  let file = input::get_contents("day14");
  let mut content = if cfg!(windows) {
    file.split("\r\n\r\n")
  } else {
    file.split("\n\n")
  };
  let polymer = content.next().unwrap().to_owned();
  let mut rules: HashMap<String, char> = HashMap::new();
  content.next().unwrap().lines().for_each(|l| {
    let mut split = l.split(" -> ");
    rules.insert(
      split.next().unwrap().to_owned(),
      split.next().unwrap().chars().nth(0).unwrap(),
    );
  });

  polymerize_smart(&poly_to_map(&polymer), &rules);
  println!("Part 1: {}", part1(&polymer, &rules));
  println!("Part 2: {}", part2(&polymer, &rules));
}

fn part1(polymer: &String, rules: &HashMap<String, char>) -> u64 {
  solve(polymer, rules, 10)
}

fn part2(polymer: &String, rules: &HashMap<String, char>) -> u64 {
  solve(polymer, rules, 40)
}

fn solve(polymer: &String, rules: &HashMap<String, char>, steps: usize) -> u64 {
  let mut p = poly_to_map(&polymer);
  for _ in 0..steps {
    p = polymerize_smart(&p, rules);
  }
  let mut count_molecules: HashMap<char, u64> = HashMap::new();
  p.iter().for_each(|(pair, count)| {
    *count_molecules
      .entry(pair.chars().nth(0).unwrap())
      .or_insert(0) += count;
  });
  *count_molecules
    .get_mut(&polymer.chars().last().unwrap())
    .unwrap() += 1;
  let mut max = 0;
  let mut min = u64::MAX;
  for n in count_molecules.into_values() {
    if n > max {
      max = n;
    }
    if n < min {
      min = n;
    }
  }
  max - min
}

fn poly_to_map(polymer: &String) -> HashMap<String, u64> {
  let mut poly_map = HashMap::new();
  for i in 0..polymer.len() - 1 {
    *poly_map.entry(polymer[i..=i + 1].to_owned()).or_insert(0) += 1;
  }
  poly_map
}

fn polymerize_smart(
  poly_map: &HashMap<String, u64>,
  rules: &HashMap<String, char>,
) -> HashMap<String, u64> {
  let mut new_poly = poly_map.clone();
  for (pattern, &molecule) in rules {
    let occurrences = *poly_map.get(pattern).unwrap_or(&0);
    if occurrences > 0 {
      let mut first = pattern.chars().nth(0).unwrap().to_string();
      first.push(molecule);
      let mut second = molecule.to_string();
      second.push(pattern.chars().nth(1).unwrap());

      *new_poly.entry(first).or_insert(0) += occurrences;
      *new_poly.entry(second).or_insert(0) += occurrences;

      *new_poly.entry(pattern.clone()).or_insert(0) -= occurrences;
    }
  }
  new_poly
}
