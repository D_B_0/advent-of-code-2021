use crate::input;

pub fn run() {
  println!("============ Day 18 ============");
  let input = input::get_contents("day18");
  println!("Part one: {}", solve_internal_p1(&input));
  println!("Part two: {}", solve_internal_p2(&input));
}

type Value = (usize, usize);

fn parse_snails(input: &String) -> Vec<Value> {
  let chars = input.chars().collect::<Vec<char>>();
  let mut depth = 0;
  let mut i = 0;
  let mut output = Vec::new();
  while i < chars.len() {
    let c = chars[i];
    if c == '[' {
      depth += 1;
      i += 1;
      continue;
    }
    if c == ']' {
      depth -= 1;
      i += 1;
      continue;
    }
    if c == ',' {
      i += 1;
      continue;
    }

    let v0 = c as i32 - '0' as i32;
    let v1 = chars[i + 1] as i32 - '0' as i32;
    let v;
    if v1 >= 0 && v1 <= 9 {
      v = v0 * 10 + v1;
      i += 1;
    } else {
      v = v0;
    }

    output.push((v as usize, depth as usize));
    i += 1;
  }

  return output;
}

fn reduce(input: &Vec<Value>) -> Vec<Value> {
  let mut result: Vec<Value> = input.clone();
  for i in 0..input.len() {
    let (value, depth) = input[i];

    if depth > 4 {
      // explode
      if i > 0 {
        (&mut result[i - 1]).0 += value;
      }
      if i + 2 < input.len() {
        (&mut result[i + 2]).0 += result[i + 1].0;
      }
      result.remove(i);
      (&mut result[i]).0 = 0;
      (&mut result[i]).1 = depth - 1;
      return result;
    }
  }

  for i in 0..input.len() {
    let (value, depth) = input[i];

    if value >= 10 {
      let v_half = value as f32 * 0.5;
      let v0 = v_half.floor() as usize;
      let v1 = v_half.ceil() as usize;
      assert_eq!(v0 + v1, value);
      (&mut result[i]).0 = v0;
      (&mut result[i]).1 = depth + 1;
      result.insert(i + 1, (v1, depth + 1));
      return result;
    }
  }

  return result;
}

fn add(a: &Vec<Value>, b: &Vec<Value>) -> Vec<Value> {
  let mut result = Vec::new();
  for &(value, depth) in a {
    result.push((value, depth + 1));
  }
  for &(value, depth) in b {
    result.push((value, depth + 1));
  }
  return result;
}

fn solve_addition(lines: &Vec<Vec<Value>>) -> Vec<Value> {
  let mut curr = lines[0].clone();

  for line in &lines[1..lines.len()] {
    let next = line;
    let mut merged = add(&curr, &next);
    let mut last = Vec::new();

    while merged != last {
      let reduced = reduce(&merged);
      last = merged.clone();
      merged = reduced;
    }
    curr = merged;
  }

  return curr;
}

fn solve_internal_addition(input: &String) -> Vec<Value> {
  let lines: Vec<_> = input
    .lines()
    .map(|v| parse_snails(&v.to_string()))
    .collect();
  return solve_addition(&lines);
}

fn calc_magnitude(input: &Vec<(usize, usize)>) -> usize {
  let mut curr = input.clone();
  let mut last = Vec::new();
  while curr != last {
    let deepest = *curr.iter().map(|(_v, d)| d).max().unwrap();
    last = curr.clone();

    if curr.len() == 1 {
      break;
    }
    for i in 0..curr.len() {
      if curr[i].1 == deepest {
        assert_eq!(curr[i].1, curr[i + 1].1);
        let left = 3 * curr[i].0;
        let right = 2 * curr[i + 1].0;
        (&mut curr[i]).0 = left + right;
        (&mut curr[i]).1 -= 1;
        curr.remove(i + 1);
        break;
      }
    }
  }

  return curr[0].0;
}

fn solve_internal_p1(input: &String) -> usize {
  return calc_magnitude(&solve_internal_addition(&input));
}

fn solve_internal_p2(input: &String) -> usize {
  let lines: Vec<_> = input.lines().map(|v| v.to_string()).collect();
  let mut max = 0;
  for i in 0..lines.len() {
    for j in 0..lines.len() {
      let line_i = parse_snails(&lines[i]);
      let line_j = parse_snails(&lines[j]);
      let lines = [line_i, line_j].to_vec();
      let mag0 = calc_magnitude(&solve_addition(&lines));
      let mag1 = calc_magnitude(&solve_addition(&lines));
      max = std::cmp::max(max, std::cmp::max(mag0, mag1));
    }
  }
  return max;
}
