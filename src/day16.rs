use crate::input;

pub fn run() {
  println!("============ Day 16 ============");
  let content = input::get_contents("day16")
    .chars()
    .map(|c| format!("{:04b}", c.to_digit(16).unwrap()))
    .collect::<Vec<String>>()
    .join("");

  let mut stream = content.chars().map(|c| c.to_digit(2).unwrap() as u64);
  let packet = parse_packet(&mut stream);
  let mut packets = vec![packet.clone()];
  let mut version_tot = 0;
  while packets.len() > 0 {
    let packet = packets.pop().unwrap();
    version_tot += packet.p_version;
    if let PacketType::Operator(_, sub_p) = packet.p_type {
      packets.extend(sub_p);
    }
  }
  println!("Part 1: {}", version_tot);
  println!("Part 2: {}", evaluate(&packet));
  // println!("{}", packet_to_string(&packet));
}

#[allow(dead_code)]
fn packet_to_string(packet: &Packet) -> String {
  match &packet.p_type {
    PacketType::Value(v) => format!("{}", v),
    PacketType::Operator(id, operands) => match id {
      0 => {
        let mut res = "(".to_owned();
        res.push_str(
          &operands
            .iter()
            .map(|p| packet_to_string(&p))
            .collect::<Vec<String>>()
            .join(" + "),
        );
        res.push(')');
        res
      }
      1 => {
        let mut res = "(".to_owned();
        res.push_str(
          &operands
            .iter()
            .map(|p| packet_to_string(&p))
            .collect::<Vec<String>>()
            .join(" * "),
        );
        res.push(')');
        res
      }
      2 => {
        let mut res = "min(".to_owned();
        res.push_str(
          &operands
            .iter()
            .map(|p| packet_to_string(&p))
            .collect::<Vec<String>>()
            .join(", "),
        );
        res.push(')');
        res
      }
      3 => {
        let mut res = "max(".to_owned();
        res.push_str(
          &operands
            .iter()
            .map(|p| packet_to_string(&p))
            .collect::<Vec<String>>()
            .join(", "),
        );
        res.push(')');
        res
      }
      5 => {
        let mut res = "(".to_owned();
        res.push_str(&packet_to_string(&operands[0]));
        res.push_str(" > ");
        res.push_str(&packet_to_string(&operands[1]));
        res.push_str(")");
        res
      }
      6 => {
        let mut res = "(".to_owned();
        res.push_str(&packet_to_string(&operands[0]));
        res.push_str(" < ");
        res.push_str(&packet_to_string(&operands[1]));
        res.push_str(")");
        res
      }
      7 => {
        let mut res = "(".to_owned();
        res.push_str(&packet_to_string(&operands[0]));
        res.push_str(" == ");
        res.push_str(&packet_to_string(&operands[1]));
        res.push_str(")");
        res
      }
      _ => unreachable!(),
    },
  }
}

fn evaluate(packet: &Packet) -> u64 {
  match &packet.p_type {
    PacketType::Value(v) => *v,
    PacketType::Operator(id, operands) => match id {
      0 => {
        // sum
        let mut sum = 0;
        for operand in operands {
          sum += evaluate(&operand);
        }
        sum
      }
      1 => {
        // product
        let mut product = 1;
        for operand in operands {
          product *= evaluate(&operand);
        }
        product
      }
      2 => {
        // minimum
        let mut min = u64::MAX;
        for operand in operands {
          min = u64::min(evaluate(&operand), min);
        }
        min
      }
      3 => {
        // maximum
        let mut max = u64::MIN;
        for operand in operands {
          max = u64::max(evaluate(&operand), max);
        }
        max
      }
      5 => {
        // greater than
        assert_eq!(operands.len(), 2);
        if evaluate(&operands[0].clone()) > evaluate(&operands[1].clone()) {
          1
        } else {
          0
        }
      }
      6 => {
        // less than
        assert_eq!(operands.len(), 2);
        if evaluate(&operands[0].clone()) < evaluate(&operands[1].clone()) {
          1
        } else {
          0
        }
      }
      7 => {
        // equal to
        assert_eq!(operands.len(), 2);
        if evaluate(&operands[0].clone()) == evaluate(&operands[1].clone()) {
          1
        } else {
          0
        }
      }
      _ => unreachable!(),
    },
  }
}

#[derive(Debug, Clone)]
enum PacketType {
  Value(u64),
  Operator(u64, Vec<Packet>),
}

#[derive(Debug, Clone)]
struct Packet {
  p_version: u64,
  p_type: PacketType,
}

fn parse_packet<I>(stream: &mut I) -> Packet
where
  I: Iterator<Item = u64>,
{
  let p_version = to_num(&take_n(stream, 3));
  let p_type = to_num(&take_n(stream, 3));
  match p_type {
    4 => {
      let mut val = Vec::new();
      let mut next_bits = take_n(stream, 5);
      val.extend_from_slice(&next_bits[1..5]);
      while next_bits[0] == 1 {
        next_bits = take_n(stream, 5);
        val.extend_from_slice(&next_bits[1..5]);
      }
      return Packet {
        p_type: PacketType::Value(to_num(&val)),
        p_version,
      };
    }
    _ => {
      let length_type_id = stream.next().unwrap();
      match length_type_id {
        0 => {
          let subpacket_len_length = 15;
          let len_bits = take_n(stream, subpacket_len_length);
          let len = to_num(&len_bits);
          let subpackets = take_n(stream, len as usize);
          let mut subpackets_stream = subpackets.into_iter();
          let mut packets = Vec::new();
          while subpackets_stream.clone().count() > 0 {
            packets.push(parse_packet(&mut subpackets_stream));
          }
          return Packet {
            p_version,
            p_type: PacketType::Operator(p_type, packets),
          };
        }
        1 => {
          let subpacket_len_length = 11;
          let len_packets = take_n(stream, subpacket_len_length);
          let len = to_num(&len_packets);
          let mut packets = Vec::new();
          for _ in 0..len {
            packets.push(parse_packet(stream));
          }
          return Packet {
            p_version,
            p_type: PacketType::Operator(p_type, packets),
          };
        }
        _ => unreachable!(),
      }
    }
  }
}

fn take_n<I>(iter: &mut I, i: usize) -> Vec<u64>
where
  I: Iterator<Item = u64>,
{
  let mut res = Vec::new();
  for _ in 0..i {
    res.push(iter.next().unwrap())
  }
  res
}

fn to_num(bits: &[u64]) -> u64 {
  let mut res = 0;
  for bit in bits {
    res = (res << 1) + bit;
  }
  res
}
