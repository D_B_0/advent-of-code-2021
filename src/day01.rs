use crate::input;

pub fn run() {
  let lines: Vec<i32> = input::get_lines("day01")
    .iter()
    .map(|x| x.parse::<i32>().unwrap())
    .collect();
  println!("============ Day 1 ============");
  println!("Part 1: {}", part1(&lines));
  println!("Part 2: {}", part2(&lines));
}

fn part1(nums: &Vec<i32>) -> i32 {
  let mut count: i32 = 0;
  for i in 1..nums.len() {
    if nums[i] > nums[i - 1] {
      count += 1;
    }
  }
  count
}

fn part2(nums: &Vec<i32>) -> i32 {
  let mut count: i32 = 0;
  let mut prev: Option<i32> = None;
  for i in 0..nums.len() - 2 {
    if prev.is_some() {
      if nums[i] + nums[i + 1] + nums[i + 2] > prev.unwrap() {
        count += 1;
      }
    }
    prev = Some(nums[i] + nums[i + 1] + nums[i + 2]);
  }
  count
}
