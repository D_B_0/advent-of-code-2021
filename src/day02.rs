use crate::input;

use regex::Regex;

#[derive(Debug)]
enum Directions {
  Forward,
  Up,
  Down,
}

struct Command {
  pub dir: Directions,
  pub ammount: i32,
}

impl Command {
  fn new(direction: Directions, amt: i32) -> Self {
    Command {
      dir: direction,
      ammount: amt,
    }
  }
}

pub fn run() {
  println!("============ Day 2 ============");

  let lines = input::get_lines("day02");
  let re = Regex::new(r"(?P<dir>forward|down|up) (?P<ammt>\d+)").unwrap();

  let mut comms = Vec::<Command>::new();
  for line in lines {
    for caps in re.captures_iter(&line) {
      match &caps["dir"] {
        "forward" => comms.push(Command::new(
          Directions::Forward,
          caps["ammt"].parse::<i32>().unwrap(),
        )),
        "down" => comms.push(Command::new(
          Directions::Down,
          caps["ammt"].parse::<i32>().unwrap(),
        )),
        "up" => comms.push(Command::new(
          Directions::Up,
          caps["ammt"].parse::<i32>().unwrap(),
        )),
        _ => panic!("Unrecognized command"),
      }
    }
  }

  println!("Part 1: {}", part1(&comms));
  println!("Part 2: {}", part2(&comms));
}

fn part1(comms: &Vec<Command>) -> i32 {
  let mut x = 0;
  let mut y = 0;
  for command in comms {
    match command.dir {
      Directions::Forward => x += command.ammount,
      Directions::Down => y += command.ammount,
      Directions::Up => y -= command.ammount,
    }
  }
  x * y
}

fn part2(comms: &Vec<Command>) -> i32 {
  let mut x = 0;
  let mut y = 0;
  let mut aim = 0;
  for command in comms {
    match command.dir {
      Directions::Forward => {
        x += command.ammount;
        y += command.ammount * aim;
      }
      Directions::Down => aim += command.ammount,
      Directions::Up => aim -= command.ammount,
    }
  }
  x * y
}
