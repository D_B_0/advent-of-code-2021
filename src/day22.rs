#![allow(dead_code, unused_variables)]
use std::{collections::HashMap, ops::RangeInclusive, vec};

use crate::input;

pub fn run() {
  println!("============ Day 22 ============");
  let content = input::get_contents_example("day22");
  println!("Part 1: {}", part1(&content));
  // println!(
  //   "{:?}",
  //   intersect_cuboids(
  //     Cuboid {
  //       on: true,
  //       xmin: 0,
  //       xmax: 10,
  //       ymin: 0,
  //       ymax: 10,
  //       zmin: 0,
  //       zmax: 10
  //     },
  //     Cuboid {
  //       on: false,
  //       xmin: -10,
  //       xmax: -5,
  //       ymin: -10,
  //       ymax: 10,
  //       zmin: -10,
  //       zmax: 10
  //     }
  //   )
  // )
}

#[derive(Copy, Clone, Debug)]
struct Cuboid {
  on: bool,
  xmin: i64,
  xmax: i64,
  ymin: i64,
  ymax: i64,
  zmin: i64,
  zmax: i64,
}

impl Cuboid {
  fn size(&self) -> i64 {
    (self.xmax - self.xmin) * (self.ymax - self.ymin) * (self.zmax - self.zmin)
  }
}

fn intersect_cuboids(a: Cuboid, b: Cuboid) -> Vec<Cuboid> {
  assert!(a.on);
  assert!(!b.on);
  if (b.xmin > a.xmax || b.xmax < a.xmin)
    || (b.ymin > a.ymax || b.ymax < a.ymin)
    || (b.zmin > a.zmax || b.zmax < a.zmin)
  {
    return vec![a];
  } else {
    let x_contained = a.xmin >= b.xmin && a.xmax <= b.xmax;
    let y_contained = a.ymin >= b.ymin && a.ymax <= b.ymax;
    let z_contained = a.zmin >= b.zmin && a.zmax <= b.zmax;

    if x_contained && y_contained && z_contained {
      return vec![];
    }

    if x_contained && y_contained && !z_contained {
      let mut new_cube = a.clone();
      if a.zmin < b.zmin {
        new_cube.zmax = b.zmin;
      } else if a.zmax > b.zmax {
        new_cube.zmin = b.zmax;
      }
      return vec![new_cube];
    }

    if x_contained && !y_contained && z_contained {
      let mut new_cube = a.clone();
      if a.ymin < b.ymin {
        new_cube.ymax = b.ymin;
      } else if a.ymax > b.ymax {
        new_cube.ymin = b.ymax;
      }
      return vec![new_cube];
    }

    if !x_contained && y_contained && z_contained {
      let mut new_cube = a.clone();
      if a.xmin < b.xmin {
        new_cube.xmax = b.xmin;
      } else if a.xmax > b.xmax {
        new_cube.xmin = b.xmax;
      }
      return vec![new_cube];
    }

    if x_contained && !y_contained && !z_contained {
      let mut new_cuboid1 = a.clone();
      let mut new_cuboid2 = a.clone();
      let mut new_cuboid3 = a.clone();
      if a.ymin < b.ymin {
        new_cuboid1.ymax = b.ymin;
        new_cuboid2.ymax = b.ymin;
        new_cuboid3.ymin = b.ymin;
      }
      if a.ymax > b.ymax {
        new_cuboid1.ymin = b.ymax;
        new_cuboid2.ymin = b.ymax;
        new_cuboid3.ymax = b.ymax;
      }
      if a.zmin < b.zmin {
        new_cuboid1.zmin = b.zmin;
        new_cuboid2.zmax = b.zmin;
        new_cuboid3.zmax = b.zmin;
      }
      if a.zmax > b.zmax {
        new_cuboid1.zmax = b.zmax;
        new_cuboid2.zmin = b.zmax;
        new_cuboid3.zmin = b.zmax;
      }
      return vec![new_cuboid1, new_cuboid2, new_cuboid3];
    }

    if !x_contained && y_contained && !z_contained {
      let mut new_cuboid1 = a.clone();
      let mut new_cuboid2 = a.clone();
      let mut new_cuboid3 = a.clone();
      if a.xmin < b.xmin {
        new_cuboid1.xmax = b.xmin;
        new_cuboid2.xmax = b.xmin;
        new_cuboid3.xmin = b.xmin;
      }
      if a.xmax > b.xmax {
        new_cuboid1.xmin = b.xmax;
        new_cuboid2.xmin = b.xmax;
        new_cuboid3.xmax = b.xmax;
      }
      if a.zmin < b.zmin {
        new_cuboid1.zmin = b.zmin;
        new_cuboid2.zmax = b.zmin;
        new_cuboid3.zmax = b.zmin;
      }
      if a.zmax > b.zmax {
        new_cuboid1.zmax = b.zmax;
        new_cuboid2.zmin = b.zmax;
        new_cuboid3.zmin = b.zmax;
      }
      return vec![new_cuboid1, new_cuboid2, new_cuboid3];
    }

    if !x_contained && !y_contained && z_contained {
      let mut new_cuboid1 = a.clone();
      let mut new_cuboid2 = a.clone();
      let mut new_cuboid3 = a.clone();
      if a.ymin < b.ymin {
        new_cuboid1.ymax = b.ymin;
        new_cuboid2.ymax = b.ymin;
        new_cuboid3.ymin = b.ymin;
      }
      if a.ymax > b.ymax {
        new_cuboid1.ymin = b.ymax;
        new_cuboid2.ymin = b.ymax;
        new_cuboid3.ymax = b.ymax;
      }
      if a.xmin < b.xmin {
        new_cuboid1.xmin = b.xmin;
        new_cuboid2.xmax = b.xmin;
        new_cuboid3.xmax = b.xmin;
      }
      if a.xmax > b.xmax {
        new_cuboid1.xmax = b.xmax;
        new_cuboid2.xmin = b.xmax;
        new_cuboid3.xmin = b.xmax;
      }
      return vec![new_cuboid1, new_cuboid2, new_cuboid3];
    }
    todo!("Implement the \"Angle intersection\" cases");
    // unreachable!();
  }
}

fn part2(content: &str) {
  let cubes: Vec<Cuboid> = content
    .lines()
    .map(|l| {
      let (on_off, rest) = l.split_once(" ").unwrap();
      let (x_range, rest) = rest.split_once(",").unwrap();
      let (y_range, z_range) = rest.split_once(",").unwrap();
      let (xmin, xmax) = parse_range_tuple(x_range);
      let (ymin, ymax) = parse_range_tuple(y_range);
      let (zmin, zmax) = parse_range_tuple(z_range);
      Cuboid {
        on: on_off == "on",
        xmin,
        xmax,
        ymin,
        ymax,
        zmin,
        zmax,
      }
    })
    .collect();
}

fn part1(content: &str) -> u64 {
  let mut map = HashMap::new();
  content
    .lines()
    .map(|l| {
      let (on_off, rest) = l.split_once(" ").unwrap();
      let (x_range, rest) = rest.split_once(",").unwrap();
      let (y_range, z_range) = rest.split_once(",").unwrap();
      (
        on_off == "on",
        parse_range(x_range),
        parse_range(y_range),
        parse_range(z_range),
      )
    })
    .for_each(|(val, x_range, y_range, z_range)| {
      for x in x_range {
        if x > 50 || x < -50 {
          continue;
        }
        for y in y_range.clone() {
          if y > 50 || y < -50 {
            continue;
          }
          for z in z_range.clone() {
            if z > 50 || z < -50 {
              continue;
            }
            let coords = (x, y, z);
            if map.contains_key(&coords) {
              *map.get_mut(&coords).unwrap() = val;
            } else {
              map.insert(coords, val);
            }
          }
        }
      }
    });
  map.iter().filter(|&(_, &v)| v).count() as u64
}

fn parse_range(string: &str) -> RangeInclusive<i64> {
  let (_, rest) = string.split_once("=").unwrap();
  let (min, max) = rest.split_once("..").unwrap();
  min.parse().unwrap()..=max.parse().unwrap()
}

fn parse_range_tuple(string: &str) -> (i64, i64) {
  let (_, rest) = string.split_once("=").unwrap();
  let (min, max) = rest.split_once("..").unwrap();
  (min.parse().unwrap(), max.parse().unwrap())
}
