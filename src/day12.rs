use crate::input;

use std::collections::{HashMap, HashSet};

pub fn run() {
  println!("============ Day 12 ============");
  let lines = input::get_lines("day12");
  let edges: Vec<(String, String)> = parse_edges(&lines);
  let caves: Vec<String> = get_caves_from_edges(&edges);

  let mut connections: HashMap<String, Vec<String>> = HashMap::new();
  for cave in &caves {
    connections.insert(cave.clone(), Vec::new());
  }
  for (c1, c2) in edges {
    connections.get_mut(&c1).unwrap().push(c2.clone());
    connections.get_mut(&c2).unwrap().push(c1.clone());
  }

  println!("Part 1: {}", part1(&connections));
  println!("Part 2: {}", part2(&connections));
}

fn part1(connections: &HashMap<String, Vec<String>>) -> u32 {
  let mut paths = vec![vec!["start".to_owned()]];
  let mut counter = 0;
  loop {
    if let Some(path) = paths.pop() {
      let last = path.last().unwrap();
      if last == "end" {
        // println!("Found path: {}", path.join(", "));
        counter += 1;
        continue;
      }
      for cave in &connections[last] {
        if is_big(cave) {
          let mut new_path = path.clone();
          new_path.push(cave.clone());
          paths.push(new_path);
        } else if !path.contains(cave) {
          let mut new_path = path.clone();
          new_path.push(cave.clone());
          paths.push(new_path);
        }
      }
    } else {
      break;
    }
  }
  counter
}

fn part2(connections: &HashMap<String, Vec<String>>) -> u32 {
  let mut paths = vec![vec!["start".to_owned()]];
  let mut counter = 0;
  loop {
    if let Some(path) = paths.pop() {
      let last = path.last().unwrap();
      if last == "end" {
        // println!("Found path: {}", path.join(", "));
        counter += 1;
        continue;
      }
      for cave in &connections[last] {
        if cave == "start" {
          continue;
        }
        if is_big(cave) {
          let mut new_path = path.clone();
          new_path.push(cave.clone());
          paths.push(new_path);
        } else if visited_small_cave_twice(&path) {
          if !path.contains(cave) {
            let mut new_path = path.clone();
            new_path.push(cave.clone());
            paths.push(new_path);
          }
        } else {
          let mut new_path = path.clone();
          new_path.push(cave.clone());
          paths.push(new_path);
        }
        // } else if path.iter().filter(|c| *c == cave).count() < 2 {
        //   let mut new_path = path.clone();
        //   new_path.push(cave.clone());
        //   paths.push(new_path);
        // }
      }
    } else {
      break;
    }
  }
  counter
}

fn is_big(cave: &String) -> bool {
  ('A'..='Z').contains(&cave.chars().nth(0).unwrap())
}

fn visited_small_cave_twice(path: &[String]) -> bool {
  for cave in path {
    if !is_big(cave) && path.iter().filter(|&c| c == cave).count() >= 2 {
      return true;
    }
  }
  false
}

fn parse_edges(lines: &[String]) -> Vec<(String, String)> {
  lines
    .iter()
    .map(|edge| {
      let mut split = edge.split('-').map(|s| s.to_string());
      (split.next().unwrap(), split.next().unwrap())
    })
    .collect()
}

fn get_caves_from_edges(edges: &[(String, String)]) -> Vec<String> {
  edges
    .iter()
    .map(|edge| vec![edge.0.clone(), edge.1.clone()])
    .flatten()
    .collect::<HashSet<String>>()
    .iter()
    .map(|cave| cave.clone())
    .collect()
}
