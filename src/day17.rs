use crate::input;

pub fn run() {
  println!("============ Day 17 ============");
  let contents = input::get_contents("day17")
    .replace("x=", "")
    .replace("y=", "");
  let ranges = contents
    .split_once(": ")
    .unwrap()
    .1
    .split_once(", ")
    .unwrap();
  let x_range_str = ranges.0.split_once("..").unwrap();
  let x_min = x_range_str.0.parse::<i32>().unwrap();
  let x_max = x_range_str.1.parse::<i32>().unwrap();
  let x_range = x_min..=x_max;
  let y_range_str = ranges.1.split_once("..").unwrap();
  let y_min = y_range_str.0.parse::<i32>().unwrap();
  let y_max = y_range_str.1.parse::<i32>().unwrap();
  let y_range = y_min..=y_max;
  // println!("X range: {:?}", x_range);
  // println!("Y range: {:?}", y_range);
  let mut y_record = 0;
  let mut velocities = vec![];
  for dx in ((2 * x_min) as f64).sqrt() as i32..=x_max {
    for dy in y_min..=-y_min {
      let mut curr_record = 0;
      let mut pos = (0, 0);
      let mut vel = (dx, dy);
      loop {
        update_pos(&mut pos, &mut vel);
        curr_record = curr_record.max(pos.1);
        // if pos.0 >= x_min && pos.0 <= x_max && pos.1 >= y_min && pos.1 <= y_max {
        if x_range.contains(&pos.0) && y_range.contains(&pos.1) {
          if !velocities.contains(&(dx, dy)) {
            velocities.push((dx, dy));
          }
          if curr_record > y_record {
            y_record = curr_record;
          }
          break;
        }
        if pos.0 > x_max {
          break;
        }
        if pos.1 < y_min {
          break;
        }
      }
    }
  }
  // for (x, y) in &velocities {
  //   print!("{},{};", x, y);
  // }
  // print!("\n");
  println!("Part 1: {}", y_record);
  println!("Part 2: {}", velocities.len());
}

type Vector = (i32, i32);

fn update_pos(pos: &mut Vector, vel: &mut Vector) {
  pos.0 += vel.0;
  pos.1 += vel.1;
  if vel.0 > 0 {
    vel.0 -= 1;
  } else if vel.0 < 0 {
    vel.0 += 1;
  }
  vel.1 -= 1;
}
