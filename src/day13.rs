use crate::input;

use colored::*;

use std::collections::HashSet;

#[derive(Debug, Clone, Copy)]
enum Fold {
  X(u32),
  Y(u32),
}

type Point = (u32, u32);

pub fn run() {
  println!("============ Day 13 ============");
  let file = input::get_contents("day13");
  let mut content = if cfg!(windows) {
    file.split("\r\n\r\n")
  } else {
    file.split("\n\n")
  };
  let mut points = collect_points(content.next().unwrap());
  let folds = collect_folds(content.next().unwrap());

  println!("Part 1: {}", resolve_fold(&points, folds[0]).len());
  for fold in folds {
    points = resolve_fold(&points, fold);
  }
  println!("Part 2: (parsing is left as an exercise for the user)");
  print_grid(&points);
  println!("{}", "Colored for your entertainement".italic());
}

fn print_grid(points: &[Point]) {
  let maxx = points
    .iter()
    .fold(0, |max, &(x, _)| if x > max { x } else { max }) as usize;
  let maxy = points
    .iter()
    .fold(0, |max, &(_, y)| if y > max { y } else { max }) as usize;
  let mut res: Vec<Vec<String>> = Vec::with_capacity(maxy as usize);
  for _ in 0..=maxy {
    let mut vec = Vec::with_capacity(maxx);
    for _ in 0..=maxx {
      vec.push(" ".to_string());
    }
    res.push(vec);
  }
  for &(x, y) in points {
    let string = match y % 6 {
      0 => "█".truecolor(255, 0, 0),
      1 => "█".truecolor(255, 127, 0),
      2 => "█".truecolor(255, 255, 0),
      3 => "█".truecolor(0, 255, 0),
      4 => "█".truecolor(0, 0, 255),
      5 => "█".truecolor(148, 0, 211),
      _ => panic!(),
    };
    res[y as usize][x as usize] = string.to_string();
  }
  print!(
    "{}",
    res
      .iter()
      .map(|l| l.join("") + "\n")
      .collect::<Vec<String>>()
      .join("")
  );
}

fn resolve_fold(points: &[Point], fold: Fold) -> Vec<Point> {
  let mut new_points: Vec<Point> = points.iter().cloned().collect();
  match fold {
    Fold::X(val) => {
      for point in new_points.iter_mut() {
        if point.0 > val {
          point.0 = 2 * val - point.0;
        }
      }
    }
    Fold::Y(val) => {
      for point in new_points.iter_mut() {
        if point.1 > val {
          point.1 = 2 * val - point.1;
          // val - (y - val)
        }
      }
    }
  }
  new_points
    .iter()
    .cloned()
    .collect::<HashSet<Point>>()
    .iter()
    .cloned()
    .collect()
}

fn collect_points(input: &str) -> Vec<Point> {
  input
    .lines()
    .map(|line| {
      let mut coords = line.split(',');
      (
        coords.next().unwrap().parse().unwrap(),
        coords.next().unwrap().parse().unwrap(),
      )
    })
    .collect()
}
fn collect_folds(input: &str) -> Vec<Fold> {
  input
    .lines()
    .map(|line| {
      let mut intermediary = line.split('=');
      match intermediary.next().unwrap().chars().last().unwrap() {
        'x' => Fold::X(intermediary.next().unwrap().parse().unwrap()),
        'y' => Fold::Y(intermediary.next().unwrap().parse().unwrap()),
        _ => panic!("error parsing folds"),
      }
    })
    .collect()
}
