use std::fs;

#[allow(dead_code)]
pub fn get_contents(file_name: &str) -> String {
  fs::read_to_string(format!("input/{}.txt", file_name)).unwrap()
}

#[allow(dead_code)]
pub fn get_lines(file_name: &str) -> Vec<String> {
  get_contents(file_name).lines().map(str::to_owned).collect()
}

#[allow(dead_code)]
pub fn get_contents_example(file_name: &str) -> String {
  fs::read_to_string(format!("example/{}.txt", file_name)).unwrap()
}

#[allow(dead_code)]
pub fn get_lines_example(file_name: &str) -> Vec<String> {
  get_contents_example(file_name)
    .lines()
    .map(str::to_owned)
    .collect()
}
