mod day01;
mod day02;
mod day03;
mod day04;
mod day05;
mod day06;
mod day07;
mod day08;
mod day09;
mod day10;
mod day11;
mod day12;
mod day13;
mod day14;
mod day15;
mod day16;
mod day17;
mod day18;
mod day19;
mod day20;
mod day21;
mod day22;
mod day23;
mod day24;
mod day25;
mod input;

const DAYS: [fn(); 25] = [
  day01::run,
  day02::run,
  day03::run,
  day04::run,
  day05::run,
  day06::run,
  day07::run,
  day08::run,
  day09::run,
  day10::run,
  day11::run,
  day12::run,
  day13::run,
  day14::run,
  day15::run,
  day16::run,
  day17::run,
  day18::run,
  day19::run,
  day20::run,
  day21::run,
  day22::run,
  day23::run,
  day24::run,
  day25::run,
];

fn main() {
  let args = std::env::args().collect::<Vec<_>>();
  if args.len() > 1 {
    if args[1] == "all" {
      let now = std::time::Instant::now();
      DAYS.iter().for_each(|day| {
        let now = std::time::Instant::now();
        day();
        println!("It took {:?}", now.elapsed());
      });
      println!("The program took {:?}", now.elapsed());
    } else {
      let day_num = args[1]
        .replace("day", "")
        .parse::<i32>()
        .expect("Pick a day number");
      if day_num <= 0 || day_num >= 26 {
        panic!("Invalid day!");
      }
      let now = std::time::Instant::now();
      DAYS.get((day_num - 1) as usize).unwrap()();
      println!("It took {:?}", now.elapsed());
    }
  } else {
    println!("Which day?");
  }
}
