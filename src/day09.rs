use crate::input;

pub fn run() {
  println!("============ Day 9 ============");
  let heightmap: Vec<Vec<u32>> = input::get_lines("day09")
    .iter()
    .map(|l| l.chars().map(|c| c.to_digit(10).unwrap()).collect())
    .collect();
  println!("Part 1: {}", part1(&heightmap));
  println!("Part 2: {}", part2(&heightmap));
}

fn part1(heightmap: &Vec<Vec<u32>>) -> u32 {
  find_risk_points(&heightmap)
    .iter()
    .map(|(x, y)| heightmap[*y][*x] + 1)
    .sum()
}

fn part2(heightmap: &Vec<Vec<u32>>) -> u32 {
  let mut heightmap_copy = heightmap.clone();
  let risk_points = find_risk_points(heightmap);
  let mut basins = Vec::<u32>::new();
  for (x, y) in risk_points {
    basins.push(floodfill(&mut heightmap_copy, x, y));
  }
  basins.sort();
  basins.reverse();
  basins[0] * basins[1] * basins[2]
}

fn floodfill(heightmap: &mut Vec<Vec<u32>>, x: usize, y: usize) -> u32 {
  let n = heightmap[y][x];
  heightmap[y][x] = 0;
  let mut result = 1;
  if x >= 1 {
    if heightmap[y][x - 1] != 9 && heightmap[y][x - 1] > n {
      result += floodfill(heightmap, x - 1, y);
    }
  }
  if x + 1 <= heightmap[0].len() - 1 {
    if heightmap[y][x + 1] != 9 && heightmap[y][x + 1] > n {
      result += floodfill(heightmap, x + 1, y);
    }
  }
  if y >= 1 {
    if heightmap[y - 1][x] != 9 && heightmap[y - 1][x] > n {
      result += floodfill(heightmap, x, y - 1);
    }
  }
  if y + 1 <= heightmap.len() - 1 {
    if heightmap[y + 1][x] != 9 && heightmap[y + 1][x] > n {
      result += floodfill(heightmap, x, y + 1);
    }
  }
  result
}

fn find_risk_points(heightmap: &Vec<Vec<u32>>) -> Vec<(usize, usize)> {
  let mut risk_points = Vec::<(usize, usize)>::new();
  for y in 0..heightmap.len() {
    for x in 0..heightmap[0].len() {
      let n = heightmap[y][x];
      let mut inferior_neighbors = 0;
      if x as i32 - 1 >= 0 {
        if heightmap[y][x - 1] <= n {
          inferior_neighbors += 1
        }
      }
      if x + 1 <= heightmap[0].len() - 1 {
        if heightmap[y][x + 1] <= n {
          inferior_neighbors += 1
        }
      }
      if y as i32 - 1 >= 0 {
        if heightmap[y - 1][x] <= n {
          inferior_neighbors += 1
        }
      }
      if y + 1 <= heightmap.len() - 1 {
        if heightmap[y + 1][x] <= n {
          inferior_neighbors += 1
        }
      }
      if inferior_neighbors == 0 {
        risk_points.push((x, y));
      }
    }
  }
  risk_points
}
