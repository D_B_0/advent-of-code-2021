use crate::input;

use std::collections::VecDeque;

pub fn run() {
  println!("============ Day 21 ============");
  let content = input::get_contents("day21");
  let mut starting_pos = content
    .lines()
    .map(|l| l.split_once(": ").unwrap().1.parse::<u32>().unwrap());
  let p1 = starting_pos.next().unwrap();
  let p2 = starting_pos.next().unwrap();
  println!("Part 1: {}", part1(p1, p2));
  println!("Part 2: {}", part2(p1, p2));
}

fn part1(p1_start: u32, p2_start: u32) -> u32 {
  let mut p1_pos = p1_start;
  let mut p1_score = 0;
  let mut p2_pos = p2_start;
  let mut p2_score = 0;
  let mut p1_turn = true;
  let mut dice = 1;
  let mut dice_count = 0;
  while p1_score < 1000 && p2_score < 1000 {
    let roll1 = dice;
    let roll2 = dice + 1;
    let roll3 = dice + 2;
    dice += 3;
    dice = wrap_to(dice, 100);
    dice_count += 3;
    if p1_turn {
      p1_pos = wrap_to(p1_pos + roll1 + roll2 + roll3, 10);
      p1_score += p1_pos;
    } else {
      p2_pos = wrap_to(p2_pos + roll1 + roll2 + roll3, 10);
      p2_score += p2_pos;
    }
    p1_turn = !p1_turn;
  }
  dice_count * if p1_score >= 1000 { p2_score } else { p1_score }
}

#[derive(Clone)]
struct Universe {
  p1_pos: u32,
  p1_score: u32,
  p2_pos: u32,
  p2_score: u32,
  p1_turn: bool,
  timelines: u64,
}

fn part2(p1_start: u32, p2_start: u32) -> u64 {
  let mut universes: VecDeque<Universe> = VecDeque::new();
  universes.push_back(Universe {
    p1_pos: p1_start,
    p1_score: 0,
    p2_pos: p2_start,
    p2_score: 0,
    p1_turn: true,
    timelines: 1,
  });
  let mut p1_won_count = 0;
  let mut p2_won_count = 0;
  while universes.len() > 0 {
    let universe = universes.pop_back().unwrap();
    if universe.p1_score >= 21 {
      p1_won_count += universe.timelines;
      continue;
    }
    if universe.p2_score >= 21 {
      p2_won_count += universe.timelines;
      continue;
    }
    for (rolls, occurances) in [(3, 1), (4, 3), (5, 6), (6, 7), (7, 6), (8, 3), (9, 1)] {
      if universe.p1_turn {
        let mut new_universe = universe.clone();
        new_universe.p1_turn = !new_universe.p1_turn;
        new_universe.p1_pos = wrap_to(new_universe.p1_pos + rolls, 10);
        new_universe.p1_score += new_universe.p1_pos;
        new_universe.timelines *= occurances;
        universes.push_back(new_universe);
      } else {
        let mut new_universe = universe.clone();
        new_universe.p1_turn = !new_universe.p1_turn;
        new_universe.p2_pos = wrap_to(new_universe.p2_pos + rolls, 10);
        new_universe.p2_score += new_universe.p2_pos;
        new_universe.timelines *= occurances;
        universes.push_back(new_universe);
      }
    }
  }
  u64::max(p1_won_count, p2_won_count)
}

fn wrap_to(num: u32, wrap: u32) -> u32 {
  (num - 1) % wrap + 1
}
