use crate::input;

pub fn run() {
  println!("============ Day 7 ============");
  let nums: Vec<u64> = input::get_contents("day07")
    .split(',')
    .map(|s| s.parse().unwrap())
    .collect();
  println!("Part 1: {}", part1(&nums));
  println!("Part 2: {}", part2(&nums));
}

fn part1(positions: &Vec<u64>) -> u64 {
  let max = *positions.iter().max().unwrap();
  (0..=max)
    .map(|pos| {
      positions
        .iter()
        .map(|&p| ((p as i64) - (pos as i64)).abs() as u64)
        .sum()
    })
    .min()
    .unwrap()
}

fn part2(positions: &Vec<u64>) -> u64 {
  let max = *positions.iter().max().unwrap();
  (0..=max)
    .map(|pos| {
      positions
        .iter()
        .map(|&p| calc_fuel(((p as i64) - (pos as i64)).abs() as u64))
        .sum()
    })
    .min()
    .unwrap()
}
fn calc_fuel(distance: u64) -> u64 {
  let mut fuel = 0;
  let mut dist = distance;
  while dist > 0 {
    fuel += dist;
    dist -= 1;
  }
  fuel
}
