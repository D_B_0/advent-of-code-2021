use crate::input;

use std::ops::RangeInclusive;

pub fn run() {
  println!("============ Day 11 ============");
  let mut grid: Vec<Vec<i32>> = input::get_contents("day11")
    .lines()
    .map(|l| l.chars().map(|c| c.to_digit(10).unwrap() as i32).collect())
    .collect();

  let mut tot_flashes = 0;
  for i in 0..usize::MAX {
    tot_flashes += step(&mut grid);
    if i == 100 {
      println!("Part 1: {}", tot_flashes);
    }
    if all_0(&grid) {
      println!("Part 2: {}", i + 1);
      return;
    }
  }
}

fn step(grid: &mut [Vec<i32>]) -> u32 {
  increment(grid);
  resolve_flashes(grid);
  let mut flashes = 0;
  for line in grid.iter_mut() {
    for e in line.iter_mut() {
      if *e == -1 {
        *e = 0;
        flashes += 1;
      }
    }
  }
  flashes
}

fn increment(grid: &mut [Vec<i32>]) {
  for line in grid.iter_mut() {
    for e in line.iter_mut() {
      *e += 1;
    }
  }
}

fn resolve_flashes(grid: &mut [Vec<i32>]) {
  while is_there_a_10(&grid) {
    for y in 0..10 {
      for x in 0..10 {
        if grid[y][x] > 9 {
          grid[y][x] = -1;
          for dy in RangeInclusive::<i32>::new(-1, 1) {
            if (y == 0 && dy == -1) || (y == 9 && dy == 1) {
              continue;
            }
            for dx in RangeInclusive::<i32>::new(-1, 1) {
              if (x == 0 && dx == -1) || (x == 9 && dx == 1) {
                continue;
              }
              if grid[(y as i32 + dy) as usize][(x as i32 + dx) as usize] >= 0 {
                grid[(y as i32 + dy) as usize][(x as i32 + dx) as usize] += 1;
              }
            }
          }
        }
      }
    }
  }
}

fn is_there_a_10(grid: &[Vec<i32>]) -> bool {
  for line in grid {
    for e in line {
      if *e > 9 {
        return true;
      }
    }
  }
  false
}

fn all_0(grid: &[Vec<i32>]) -> bool {
  for line in grid {
    for e in line {
      if e != &0 {
        return false;
      }
    }
  }
  true
}

#[allow(dead_code)]
fn print_grid(grid: &[Vec<i32>]) {
  for line in grid {
    for spot in line {
      print!("{:2}", spot);
    }
    print!("\n");
  }
}
