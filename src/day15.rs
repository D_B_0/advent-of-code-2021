use crate::input;

use std::collections::{HashMap, VecDeque};

use colored::*;

pub fn run() {
  println!("============ Day 15 ============");
  let map: Vec<Vec<u32>> = input::get_lines("day15")
    .iter()
    .map(|l| {
      l.chars()
        .map(|c| c.to_digit(10).unwrap())
        .collect::<Vec<u32>>()
    })
    .collect();

  println!("Part 1: {}", solve(&map, false));

  let map_height = map.len();
  let map_width = map[0].len();
  let big_map_height = map_height * 5;
  let big_map_width = map_width * 5;
  let mut big_map: Vec<Vec<u32>> = Vec::with_capacity(big_map_height);
  for y in 0..big_map_height {
    let mut line = Vec::with_capacity(big_map_width);

    let y_ratio = y / map_height;
    for x in 0..big_map_width {
      let x_ratio = x / map_width;
      let ratio = x_ratio + y_ratio;
      let value = map[y % map_height][x % map_width] + ratio as u32;
      if value > 9 {
        line.push(value - 9);
      } else {
        line.push(value);
      }
    }
    big_map.push(line);
  }
  println!("Part 2: {}", solve(&big_map, false));
}

fn solve(map: &[Vec<u32>], print: bool) -> u32 {
  let path = a_star(&map, (0, 0), (map[0].len() - 1, map.len() - 1));
  if print {
    for y in 0..map.len() {
      for x in 0..map[0].len() {
        if path.contains(&(x, y)) {
          print!("{}", map[y][x].to_string().bold().red());
        } else {
          print!("{}", map[y][x]);
        }
      }
      println!("");
    }
  }
  let mut res = 0;
  for i in 1..path.len() {
    let (x, y) = path[i];
    res += map[y][x];
  }
  res
}

type Spot = (usize, usize);

fn a_star(map: &[Vec<u32>], start: Spot, goal: Spot) -> Vec<Spot> {
  let mut open_set: VecDeque<Spot> = VecDeque::from(vec![start]);
  let mut came_from: HashMap<Spot, Spot> = HashMap::new();
  let mut g_score: HashMap<Spot, u32> = HashMap::new();
  g_score.insert(start, 0);
  let mut f_score: HashMap<Spot, u32> = HashMap::new();
  f_score.insert(start, manhattan_distance(start, goal));

  while open_set.len() != 0 {
    let mut curr: Option<Spot> = None;
    let mut min_score = u32::MAX;
    for spot in &open_set {
      let score = *f_score.get(spot).unwrap_or(&u32::MAX);
      if score < min_score {
        curr = Some(*spot);
        min_score = score;
      }
    }
    let current = curr.unwrap();
    if current == goal {
      return reconstruct_path(&came_from, goal);
    }
    open_set.remove(open_set.iter().position(|&s| s == current).unwrap());
    let mut neighbors: Vec<Spot> = Vec::new();
    if current.0 > 0 {
      neighbors.push((current.0 - 1, current.1));
    }
    if current.0 < map[0].len() - 1 {
      neighbors.push((current.0 + 1, current.1));
    }
    if current.1 > 0 {
      neighbors.push((current.0, current.1 - 1));
    }
    if current.1 < map.len() - 1 {
      neighbors.push((current.0, current.1 + 1));
    }
    let current_g_score = g_score[&current];
    for neighbor in neighbors {
      let neighbor_g_score = *g_score.get(&neighbor).unwrap_or(&u32::MAX);
      let tentative_g_score = current_g_score + map[neighbor.1][neighbor.0];
      if tentative_g_score < neighbor_g_score {
        came_from.insert(neighbor, current);
        g_score.insert(neighbor, tentative_g_score);
        f_score.insert(
          neighbor,
          tentative_g_score + manhattan_distance(neighbor, goal),
        );
        if !open_set.contains(&neighbor) {
          open_set.push_back(neighbor);
        }
      }
    }
  }
  unreachable!();
}

fn reconstruct_path(came_from: &HashMap<Spot, Spot>, current: Spot) -> Vec<Spot> {
  let mut path = vec![current];
  let mut curr = current;
  while came_from.contains_key(&curr) {
    curr = came_from[&curr];
    path.push(curr);
  }
  path.reverse();
  path
}

fn manhattan_distance(curr: Spot, target: Spot) -> u32 {
  (((curr.0 as i32) - (target.0 as i32)).abs() + ((curr.1 as i32) - (target.1 as i32)).abs()) as u32
}
